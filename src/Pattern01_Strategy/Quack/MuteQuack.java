package Pattern01_Strategy.Quack;

/**
 * ���������� ��������� ��� ����, ������� �� �������
 * Created by a.voronin on 15.10.2015.
 */
public class MuteQuack implements QuackBehavior {

	@Override
	public void quack() {                               // ���������� ��������
		System.out.println("<< SILENCE >>");
	}
}
