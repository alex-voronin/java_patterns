package Pattern01_Strategy.Quack;

/**
 * ���������� �������� ��� ����, ������� ������ Squeak
 * Created by a.voronin on 15.10.2015.
 */
public class Squeak implements QuackBehavior{

	@Override
	public void quack() {
		System.out.println("Squeak!");
	}
}
