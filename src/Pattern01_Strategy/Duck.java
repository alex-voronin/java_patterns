package Pattern01_Strategy;

import Pattern01_Strategy.Fly.FlyBehavior;
import Pattern01_Strategy.Quack.QuackBehavior;

/**
 * ����������� ����� ����
 * Created by a.voronin on 14.10.2015.
 */
public abstract class Duck {
	FlyBehavior flyBehavior;      // ��������� ������
	QuackBehavior quackBehavior;  // ��������� ��������

	public Duck() {                   // �����������

	}

	public abstract void display(); // ����������� ����� ����������� ����, �� ����������


	public void performFly() {      // ����� ���� ���������� ����� ��������� ������ flyBehavior
		flyBehavior.fly();
	}

	public void performQuack() {    // �������� ����, ����������� ����� ��������� �������� quackBehavior
		quackBehavior.quack();
	}

	public void swim() {               // �������� ���� ����������� ����
		System.out.println("All ducks float, even decoys!");
	}


	/**
	 * ������ ��� ��������� ���������� ��������� ������
	 */
	public void setFlyBehavior(FlyBehavior fb) {
		flyBehavior = fb;
	}

	/**
	 * ������ ��� ��������� ���������� ��������� ��������
	 */
	public void setQuackBehavior(FlyBehavior qb) {
		flyBehavior = qb;
	}

}
