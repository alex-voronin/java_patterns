package Pattern01_Strategy;

import Pattern01_Strategy.Fly.FlyWithWings;
import Pattern01_Strategy.Quack.Quack;

/**
 * �����, ����������� ����, �� ��������� ������������ ������ ����
 * Created by a.voronin on 14.10.2015.
 */
public class MallardDuck extends Duck {

	public MallardDuck(){               // ����������� MallardDuck

		// ����� ������ ����������� ��������� FlyWithWings - �������� ����
		flyBehavior = new FlyWithWings();   // ����������� ��������� ������

		// �������� ������� ����������� ��������� Quack - ��������� ����
		quackBehavior = new Quack();        // ����������� ��������� ��������

	}

	public void display(){
		System.out.println("I'm real mallard duck!");   // ����������� ����������� ����
	}

}
