package Pattern01_Strategy.Fly;

/**
 * ���������� ��������� ��� ����, ������� �� ������
 * Created by a.voronin on 15.10.2015.
 */
public class FlyNoWay implements FlyBehavior {

	@Override
	public void fly() {
		System.out.println("I can't fly!! :( ");
	}
}
