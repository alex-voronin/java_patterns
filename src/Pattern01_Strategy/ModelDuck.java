package Pattern01_Strategy;

import Pattern01_Strategy.Fly.FlyNoWay;
import Pattern01_Strategy.Quack.Quack;

/**
 * ���� ��������, ���������� ������ �� �����
 * Created by a.voronin on 15.10.2015.
 */
public class ModelDuck extends Duck {

	public ModelDuck() {
		flyBehavior = new FlyNoWay();     // ���������� ������ �� �����
		quackBehavior = new Quack();      // �� ����� �������
	}

	@Override
	public void display() {
		System.out.println("I'm a model duck!");
	}
}
