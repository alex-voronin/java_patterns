package Pattern06_01_Command;

import Pattern06_01_Command.Door.Door;
import Pattern06_01_Command.Door.GarageDoorOpenCommand;
import Pattern06_01_Command.Light.Light;
import Pattern06_01_Command.Light.LightOnCommand;

/**
 * Created by a.voronin on 21.10.2015.
 */
public class RemoteControlTest {
	public static void main(String[] args) {

		// ������ ������, ��� ��������� ������ �������
		SimpleRemoteControl remote = new SimpleRemoteControl();

		// ������ Light - ���������� �������
		Light light = new Light("����");
		// �������� ������� � ��������� ����������
		LightOnCommand lightOn = new LightOnCommand(light);

		remote.setCommand(lightOn);     // ������������� �������
		remote.buttonWasPressed();      // ���������

		Door gDoor = new Door();
		GarageDoorOpenCommand gDoorCom = new GarageDoorOpenCommand(gDoor);

		remote.setCommand(gDoorCom);
		remote.buttonWasPressed();


	}

}
