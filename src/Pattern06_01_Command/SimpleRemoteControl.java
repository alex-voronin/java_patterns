package Pattern06_01_Command;

/**
 * �����
 * Created by a.voronin on 21.10.2015.
 */
public class SimpleRemoteControl {
	Command slot;

	public SimpleRemoteControl() {

	}

	public void setCommand(Command command) {
		slot = command;
	}

	public void buttonWasPressed() {
		slot.execute();
	}
}
