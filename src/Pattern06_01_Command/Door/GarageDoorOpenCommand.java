package Pattern06_01_Command.Door;

import Pattern06_01_Command.Command;


/**
 * Created by a.voronin on 21.10.2015.
 */
public class GarageDoorOpenCommand implements Command {
	Door door;

	public GarageDoorOpenCommand(Door door) {
		// ����������� ������� ����������
		this.door = door;
	}

	public void execute() {
		// ��������� ������� � ����������
		door.open();
	}
}
