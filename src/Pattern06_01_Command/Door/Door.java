package Pattern06_01_Command.Door;

/**
 * Created by a.voronin on 21.10.2015.
 */
public class Door {

	public Door() {

	}

	public void open() {
		System.out.println("Door is open");
	}

	public void close() {
		System.out.println("Door was close");
	}
}
