package Pattern06_01_Command.Light;

import Pattern06_01_Command.Command;

/**
 * ������� �� ���������� �����
 * Created by a.voronin on 21.10.2015.
 */
public class LightOffCommand implements Command {
	Light light;

	public LightOffCommand(Light light) {
		// ����������� ������� ����������
		this.light = light;
	}


	@Override
	public void execute() {
		// ��������� ������� � ����������
		light.off();
	}
}
