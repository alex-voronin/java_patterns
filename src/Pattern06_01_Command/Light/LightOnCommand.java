package Pattern06_01_Command.Light;

import Pattern06_01_Command.Command;

/**
 * ������� �� ��������� �����
 * Created by a.voronin on 21.10.2015.
 */
public class LightOnCommand implements Command {
	Light light;

	public LightOnCommand(Light light) {
		// ����������� ������� ����������
		this.light = light;
	}

	public void execute() {
		// ��������� ������� � ����������
		light.on();
	}
}
