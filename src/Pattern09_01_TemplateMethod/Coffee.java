package Pattern09_01_TemplateMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * ����
 * Created by a.voronin on 22.10.2015.
 */
public class Coffee extends CaffeineBeverage {

	/*public void prepareRecipe() {
		boilWater();
		brewCoffeeGrinds();
		pourInCup();
		addSugarAndMilk();
	}*/

	/*public void boilWater() {
		System.out.println("Boiling water");
	}*/

	public void brew() {
		System.out.println("���������� ���� ����� ������");
	}

/*	public void pourInCup() {
		System.out.println("Pouring into cup");
	}*/

	public void addCondiments() {
		System.out.println("��������� ����� � ������");
	}

	/**
	 * �������������� �����, ���������� � �������, ����� �� ��������� �������
	 *
	 * @return
	 */
	public boolean customerWantsCondiments() {
		String answer = getUserInput();
		if (answer.toLowerCase().startsWith("y")) {
			return true;
		} else {
			return false;
		}
	}


	private String getUserInput() {
		String answer = null;
		System.out.print("Would you like milk and sugar with your coffee (y/n)? ");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		try {
			answer = in.readLine();
		} catch (IOException ioe) {
			System.err.println("IO error trying to read your answer");
		}
		if (answer == null) {
			return "no";
		}
		return answer;
	}
}
