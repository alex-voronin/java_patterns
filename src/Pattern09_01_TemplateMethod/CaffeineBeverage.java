package Pattern09_01_TemplateMethod;

/**
 * ����������� ����� ����������� �������
 * Created by a.voronin on 22.10.2015.
 */
public abstract class CaffeineBeverage {
	final void prepareRecipe() {
		boilWater();
		brew();
		pourInCup();
		if (customerWantsCondiments()) {
			addCondiments();
		}

	}

	abstract void brew();

	abstract void addCondiments();

	void boilWater() {
		System.out.println("�������� ����� ����");
	}

	void pourInCup() {
		System.out.println("�������� ��������� ��������� � �����");
	}


	/**
	 * �������� ����� �������������� ���� �����, �� �� ������ ��� ������
	 *
	 * @return
	 */
	boolean customerWantsCondiments() {
		return true;
	}

}
