package Pattern09_01_TemplateMethod;

/**
 * Created by a.voronin on 22.10.2015.
 */
public class Test {
	public static void main(String[] args) {
		Tea tea = new Tea();
		Coffee coffee = new Coffee();


		System.out.println("Приготовим чайку....");
		tea.prepareRecipe();

		System.out.println();

		System.out.println("Приготовим кофейку....");
		coffee.prepareRecipe();
	}
}
