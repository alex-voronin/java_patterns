package Pattern07_01_Adapter;

import java.util.ArrayList;

/**
 * ��������� �������
 * Created by a.voronin on 22.10.2015.
 */
public class DuckTestDrive {
	public static void main(String[] args) {

		MallardDuck duck = new MallardDuck();   // ������� ����

		WildTurkey turkey = new WildTurkey();
		Duck turkeyAdapter = new TurkeyAdapter(turkey);

		System.out.println("The Turkey says...");
		turkey.gobble();
		turkey.fly();

		System.out.println("\nThe Duck says...");
		testDuck(duck);

		System.out.println("\nThe TurkeyAdapter says...' ");
		testDuck(turkeyAdapter);

		/*ArrayList list = new ArrayList();
		list.add(1);
		list.add(2);
		list.forEach((lisst) -> {
			System.out.println(lisst.toString());
		});*/

	}


	static void testDuck(Duck duck) {
		duck.quack();
		duck.fly();
	}

}
