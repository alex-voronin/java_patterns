package Pattern07_01_Adapter;

/**
 * Created by a.voronin on 22.10.2015.
 */
public class MallardDuck implements Duck {
	@Override
	public void quack() {
		System.out.println("QUACK!");
	}

	@Override
	public void fly() {
		System.out.println("I can fly!!!!");
	}
}
