package Pattern07_01_Adapter;

/**
 * Created by a.voronin on 22.10.2015.
 */
public interface Duck {

	public void quack();

	public void fly();

}
