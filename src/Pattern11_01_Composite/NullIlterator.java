package Pattern11_01_Composite;

import java.util.Iterator;

/**
 * Created by a.voronin on 28.10.2015.
 */
public class NullIlterator implements Iterator {
	@Override
	public Object next() {
		return null;
	}

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
