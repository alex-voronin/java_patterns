package Pattern11_01_Composite;

 
import Pattern11_01_Composite.Menu.Menu;
import Pattern11_01_Composite.Menu.MenuComponent;

import java.util.*;
  
public class CompositeIterator implements Iterator {
	Stack stack = new Stack();
   
	public CompositeIterator(Iterator iterator) {
		// �������� �������� �������� ������, ��������� ��� � �����
		stack.push(iterator);
	}
   
	public Object next() {
		// ����� ������������� ���� ������� - ������� ��������� ��� �������������
		if (hasNext()) {
			// ���� ������� �������� ����, �� ��������� ��� � �����. ����. �������
			Iterator iterator = (Iterator) stack.peek();
			MenuComponent component = (MenuComponent) iterator.next();
			if (component instanceof Menu) {
				// ���� ��������� ��������� � ������ Menu, �� ���� ��� ���� �������� �
				// �������, ������� � ���� �������� ��� �������
				stack.push( ((Menu) component).createIterator() );
			}
			return component;
		} else {
			return null;
		}
	}


	public boolean hasNext() {
		// ��� �������� ������� ���� ���������� - ��������� ���� �� ����
		//���� ��� - ��������� � ������ ��������, ��������� ���� �� ���� �������
		// ���� ��� - ��������� �������� �� ����� � ���������� �������� hasNext
		if (stack.empty()) {
			return false;
		} else {
			Iterator iterator = (Iterator) stack.peek();
			if (!iterator.hasNext()) {
				stack.pop();
				return hasNext();
			} else {
				return true;
			}
		}
	}
   
	public void remove() {
		throw new UnsupportedOperationException();
	}
}


