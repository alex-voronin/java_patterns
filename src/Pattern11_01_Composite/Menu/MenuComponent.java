package Pattern11_01_Composite.Menu;

import java.util.Iterator;

/**
 * ����������� ����� ��� ����
 * Created by a.voronin on 27.10.2015.
 */
public abstract class MenuComponent {

	public void add(MenuComponent menuComponent){
		throw new UnsupportedOperationException();
	}

	public void remove(MenuComponent menuComponent){
		throw new UnsupportedOperationException();
	}

	public MenuComponent getChild(int i){
		throw new UnsupportedOperationException();
	}

	public String GetName(){
		throw new UnsupportedOperationException();
	}

	public String GetDescription(){
		throw new UnsupportedOperationException();
	}

	public double getPrice(){
		throw new UnsupportedOperationException();
	}

	public boolean isVegetarian() {
		throw new UnsupportedOperationException();
	}

	public void print() {
		throw new UnsupportedOperationException();
	}

	public abstract Iterator createIterator();

}
