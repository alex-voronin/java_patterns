package Pattern03_01_Decorator.Decorators;

import Pattern03_01_Decorator.Drinks.Beverage;

/**
 * ����� ���������� Mocha (������� � ���������)
 * Created by a.voronin on 15.10.2015.
 */
public class Mocha extends CondimentDecorator {
	Beverage beverage;    // ���������� ��� �������� ������

	public Mocha(Beverage beverage) {
		this.beverage = beverage;       // ��������� ������ �� ������
	}

	public String getDescription() {
		return beverage.getDescription() + ", Mocha";  // ��������� �������� �������
	}

	public Double cost() {
		Double val = 0.20 + beverage.cost();
		return val;
	}
}
