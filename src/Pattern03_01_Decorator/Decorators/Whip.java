package Pattern03_01_Decorator.Decorators;

import Pattern03_01_Decorator.Drinks.Beverage;

/**
 * Created by a.voronin on 15.10.2015.
 */
public class Whip extends CondimentDecorator {
	Beverage beverage;    // ���������� ��� �������� ������

	public Whip(Beverage beverage) {
		this.beverage = beverage;       // ��������� ������ �� ������
	}

	public String getDescription() {
		return beverage.getDescription() + ", Whip";  // ��������� �������� �������
	}

	public Double cost() {
		Double val = 0.40 + beverage.cost();
		return val;
	}
}
