package Pattern03_01_Decorator.Decorators;

import Pattern03_01_Decorator.Drinks.Beverage;

/**
 * Created by a.voronin on 15.10.2015.
 */
public class Soy extends CondimentDecorator {
	Beverage beverage;    // ���������� ��� �������� ������

	public Soy(Beverage beverage) {
		this.beverage = beverage;       // ��������� ������ �� ������
	}

	public String getDescription() {
		return beverage.getDescription() + ", Soy";  // ��������� �������� �������
	}

	public Double cost() {
		Double val = 0.10 + beverage.cost();
		return val;
	}
}
