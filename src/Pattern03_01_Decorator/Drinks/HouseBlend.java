package Pattern03_01_Decorator.Drinks;

/**
 * Created by a.voronin on 15.10.2015.
 */
public class HouseBlend extends Beverage {

	public HouseBlend() {
		description = "House Blend Coffee";
	}

	public Double cost() {
		return 0.89;
	}
}
