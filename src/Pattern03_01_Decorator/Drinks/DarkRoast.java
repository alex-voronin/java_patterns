package Pattern03_01_Decorator.Drinks;

/**
 * Created by a.voronin on 15.10.2015.
 */
public class DarkRoast extends Beverage {
	public DarkRoast() {
		description = "Dark Roast";
	}

	public Double cost() {
		return 0.99;
	}
}
