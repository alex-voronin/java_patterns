package Pattern03_01_Decorator.Drinks;

/**
 * ����������� ����� �������
 * Created by a.voronin on 15.10.2015.
 */
public abstract class Beverage {
	String description = "Unknow Beverage";

	// �����, ������������ �������� ��� ����������
	public String getDescription() {
		return description;
	}

	// �����, ������������ ��������� ����� ����������� � ����������
	public abstract Double cost();
}
