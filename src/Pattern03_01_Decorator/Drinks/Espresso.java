package Pattern03_01_Decorator.Drinks;

/**
 * ��������
 * Created by a.voronin on 15.10.2015.
 */
public class Espresso extends Beverage {

	public Espresso() {
		description = "Espresso";
	}

	public Double cost() {
		return 2.0;
	}

}
