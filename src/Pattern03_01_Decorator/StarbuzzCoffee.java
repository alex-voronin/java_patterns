package Pattern03_01_Decorator;

import Pattern03_01_Decorator.Decorators.Mocha;
import Pattern03_01_Decorator.Decorators.Soy;
import Pattern03_01_Decorator.Decorators.Whip;
import Pattern03_01_Decorator.Drinks.Beverage;
import Pattern03_01_Decorator.Drinks.Espresso;

/**
 * ��������� ������������� ����
 * Created by a.voronin on 15.10.2015.
 */
public class StarbuzzCoffee {
	public static void main(String[] args) {

		Beverage beverage = new Espresso();
		System.out.println(beverage.getDescription() + " $" + beverage.cost());
		System.out.println();

		Beverage beverage2 = new Espresso();
		beverage2 = new Mocha(beverage2);
		beverage2 = new Mocha(beverage2);
		//beverage2 = new Whip(beverage2);
		double val = Math.nextUp(beverage2.cost()) ;
		System.out.println(beverage2.getDescription() + " $" + val);
		System.out.println();

		Beverage beverage3 = new Espresso();
		beverage3 = new Soy(beverage3);
		beverage3 = new Mocha(beverage3);
		beverage3 = new Whip(beverage3);
		System.out.println(beverage3.getDescription() + " $" + beverage3.cost());
		System.out.println();

		// new feature for test_branch4
		// add to git master
		 //|||

		// commit for new local branch 01 gdfgdfgdfg
		// master 02

		//new feature for new local branch 02
		//new feature for new local branch 02 01

		//new feature for new local branch 03 ________________________________

		// �������

	}
}
