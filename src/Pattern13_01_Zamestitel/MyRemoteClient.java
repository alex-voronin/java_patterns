package Pattern13_01_Zamestitel;

import java.rmi.Naming;

/**
 * ���������� ���
 * Created by a.voronin on 02.11.2015.
 */
public class MyRemoteClient {
	public static void main(String[] args) {
		new MyRemoteClient().go();
	}

	public void go() {
		try {
			MyRemote service = (MyRemote) Naming.lookup("rmi://121.0.0.1/RemoteHello ");
			String s = service.sayHello();
			System.out.println(s);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
