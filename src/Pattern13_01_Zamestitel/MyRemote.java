package Pattern13_01_Zamestitel;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * ��������� ���������� �������
 * Created by a.voronin on 02.11.2015.
 */
public interface MyRemote extends Remote {
	public String sayHello() throws RemoteException;
}
