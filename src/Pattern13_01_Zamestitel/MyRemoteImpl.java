package Pattern13_01_Zamestitel;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * ���������� ���������� ���������� �������
 * Created by a.voronin on 02.11.2015.
 */
public class MyRemoteImpl extends UnicastRemoteObject implements MyRemote {

	public MyRemoteImpl() throws RemoteException {
	}

	public static void main(String[] args) {
		try {
			MyRemote service = new MyRemoteImpl();
			Naming.rebind("RemoteHello", service);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public String sayHello() throws RemoteException {
		return "Server says, 'Hey!' ";
	}
}
