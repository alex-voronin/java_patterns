package Pattern10_01_Iterator;

/**
 * Created by a.voronin on 23.10.2015.
 */
public interface Iterator {

	boolean hasNext();

	Object next();
}
