package Pattern10_01_Iterator.Diner;

import Pattern10_01_Iterator.Iterator;
import Pattern10_01_Iterator.MenuItem;

/**
 * Created by a.voronin on 23.10.2015.
 */
public class DinerMenuIterator implements Iterator {
	MenuItem[] items;
	int position = 0;


	public DinerMenuIterator(MenuItem[] items) {
		this.items = items;
	}

	/**
	 * ��������� ���� �� ��� �������� �������
	 *
	 * @return
	 */
	@Override
	public boolean hasNext() {
		if (position >= items.length || items[position] == null) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * ���������� ��������� ������� �������
	 *
	 * @return
	 */
	@Override
	public Object next() {
		MenuItem menuItem = items[position];
		position = position + 1;
		return menuItem;

	}

}
