package Pattern10_01_Iterator.Pancake;

import Pattern10_01_Iterator.Iterator;
import Pattern10_01_Iterator.MenuItem;

import java.util.ArrayList;

/**
 * ���� �������
 * Created by a.voronin on 23.10.2015.
 */
public class PancakeHouseMenu {
	ArrayList menuItems;        // ������ �������� ���� � ArrayList

	public PancakeHouseMenu() {
		menuItems = new ArrayList();

		addItem("K&B's Pancake Breakfast",
				"Pancakes with scrambled eggs, and toast",
				true,
				2.99);

		addItem("Regular Pancake Breakfast",
				"Pancakes with fried eggs, sausage",
				false,
				2.99);

		addItem("Blueberry Pancakes",
				"Pancakes made with fresh blueberries",
				true,
				3.49);

		addItem("Waffles",
				"Waffles, with your choice of blueberries or strawberries",
				true,
				3.59);
	}


	public void addItem(String name, String description, boolean vegetarian, double price) {
		MenuItem menultem = new MenuItem(name, description, vegetarian, price);
		menuItems.add(menultem);
	}



	public Iterator createIterator() {
		return new PancakeHouseMenuIterator(menuItems);
	}
}
