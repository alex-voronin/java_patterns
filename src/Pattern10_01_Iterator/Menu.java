package Pattern10_01_Iterator;

import java.util.Iterator;

public interface Menu {
	public Iterator createIterator();
}
