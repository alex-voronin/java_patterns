package Pattern02_01_Observer;

/**
 * ��������� �����������
 * Created by a.voronin on 15.10.2015.
 */
public interface Observer {
	public void update(float temp, float humidity, float pressure);
}
