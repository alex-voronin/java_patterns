package Pattern02_01_Observer;

/**
 * Ёлемент, отображающий данные
 * Created by a.voronin on 15.10.2015.
 */
public interface DisplayElement {
	public void display();    // отображение

}
