package Pattern02_01_Observer;

/**
 * ����� �������� ������ �� WeatherData
 * Created by a.voronin on 15.10.2015.
 */
public class CurrentDisplay implements  Observer, DisplayElement {
	private float temperature = 0;
	private float humidity = 0;
	private Subject weatherData;

	public CurrentDisplay(Subject weatherData) {
		// �������� ������ WeatherData ��� ����������� �������� � �������� �����������
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
		weatherData = null;
	}

	public void update(float temperature, float humidity, float pressure) {
		this.temperature = temperature;     // ��������� ��������, �����
		this.humidity = humidity;
		display();                          // ��������
	}

	public void display() {
		System.out.println("Current conditions: " + temperature
				+ "F degrees and " + humidity + "% humidity");
	}

}
