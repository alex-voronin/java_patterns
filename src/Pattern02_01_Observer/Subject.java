package Pattern02_01_Observer;

/**
 * ��������� ��������
 * Created by a.voronin on 15.10.2015.
 */
public interface Subject {
	public void registerObserver(Observer o);   // �������������� �����������
	public void removeObserver(Observer o);     // ������� �����������
	public void notifyObservers();              // ��������� ������������ �����������
}
