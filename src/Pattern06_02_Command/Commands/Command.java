package Pattern06_02_Command.Commands;

/**
 * Created by a.voronin on 21.10.2015.
 */
public interface Command {

	public  void execute();     // ���������� �������

	public  void undo();        // ������ ���������� ��������

}
