package Pattern06_02_Command.Commands;

/**
 * ������� �� ��������� �����
 * Created by a.voronin on 21.10.2015.
 */
public class LightOnCommand implements Command {
	Light light;

	public LightOnCommand(Light light) {
		this.light = light; // ����������� ������� ���������� �������
	}

	public void execute() {
		// ��������� ������� � ����������
		light.on();
	}

	public void undo() {
		light.off(); //
	}
}
