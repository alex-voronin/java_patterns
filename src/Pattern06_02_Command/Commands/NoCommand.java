package Pattern06_02_Command.Commands;

import Pattern06_02_Command.Commands.Command;

/**
 * Created by a.voronin on 21.10.2015.
 */
public class NoCommand implements Command {
	public void execute() { }

	public void undo() { }
}
