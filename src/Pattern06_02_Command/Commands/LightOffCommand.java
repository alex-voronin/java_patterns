package Pattern06_02_Command.Commands;

/**
 * ������� �� ���������� �����
 * Created by a.voronin on 21.10.2015.
 */
public class LightOffCommand implements Command {
	Light light;

	public LightOffCommand(Light light) {
		// ����������� ������� ����������
		this.light = light;
	}


	public void execute() {
		// ��������� ������� � ����������
		light.off();
	}

	public void undo() {
		light.on(); //
	}
}
