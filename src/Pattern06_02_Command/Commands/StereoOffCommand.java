package Pattern06_02_Command.Commands;

public class StereoOffCommand implements Command {
	Stereo stereo;

	public StereoOffCommand(Stereo stereo) {
		this.stereo = stereo; // присваиваем команде получателя команды
	}

	public void execute() {
		stereo.off(); //
	}

	public void undo() {
		stereo.off();
	}
}
