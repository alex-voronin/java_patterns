package Pattern14_01_PatternsHell_MVC_start;


/**
 * ����������� ���� ������ (Mallard)
 * Created by a.voronin on 09.11.2015.
 */
public class MallardDuck implements Quackable {
	Observable observable;

	public MallardDuck() {
		observable = new Observable(this);
	}

	@Override
	public void quack() {
		System.out.println("Quack! Quack!");
		notifyObservers();
	}

	@Override
	public void registerObserver(Observer observer) {
		observable.registerObserver(observer);
	}

	@Override
	public void notifyObservers() {
		observable.notifyObservers();
	}

	@Override
	public String toString() {
		return "MallardDuck";
	}
}
