package Pattern14_01_PatternsHell_MVC_start;

/**
 * ���������� ����������� ������ �������
 * ������ ����� ������� ���� ������������� ����
 * Created by a.voronin on 09.11.2015.
 */
public class DuckFactory extends AbstractDuckFactory {
	@Override
	public Quackable createMallardDuck() {
		return new MallardDuck();
	}

	@Override
	public Quackable createRedheadDuck() {
		return new RedheadDuck();
	}

	@Override
	public Quackable createDuckCall() {
		return new DuckCall();
	}

	@Override
	public Quackable createRubberDuck() {
		return new RubberDuck();
	}
}
