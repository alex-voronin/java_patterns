package Pattern14_01_PatternsHell_MVC_start;

/**
 * ����������� ������� ��� ����
 * ������ ����� ������� ���� �� �������������� ����
 * Created by a.voronin on 09.11.2015.
 */
public abstract class AbstractDuckFactory {
	public abstract Quackable createMallardDuck();

	public abstract Quackable createRedheadDuck();

	public abstract Quackable createDuckCall();

	public abstract Quackable createRubberDuck();
}
