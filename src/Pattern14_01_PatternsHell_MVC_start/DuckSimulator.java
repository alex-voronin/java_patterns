package Pattern14_01_PatternsHell_MVC_start;

/**
 * ��������� ���� :)
 * Created by a.voronin on 09.11.2015.
 */
public class DuckSimulator {

	public static void main(String[] args) {
		DuckSimulator simulator = new DuckSimulator();
		AbstractDuckFactory duckFactory = new CountingDuckFactory();
		simulator.simulate(duckFactory);   // �������� ��������� ������������ ���� )))
	}

	public void simulate(AbstractDuckFactory duckFactory) {
		Quackable mallardDuck = duckFactory.createMallardDuck();
		Quackable redheadDuck = duckFactory.createRedheadDuck();
		Quackable duckCall = duckFactory.createDuckCall();
		Quackable rubberDuck = duckFactory.createRubberDuck();
		Quackable gooseDuck = new GooseAdapter(new Goose());

		System.out.println("<-Duck Simulator With Composite � Flocks->");

		// ������� ����
		Flock flockDucks = new Flock();
		flockDucks.add(redheadDuck);
		flockDucks.add(duckCall);
		flockDucks.add(rubberDuck);
		flockDucks.add(gooseDuck);

		Flock flockOfMallrds = new Flock();
		Quackable mallardOne    = duckFactory.createMallardDuck();
		Quackable mallardTwo    = duckFactory.createMallardDuck();
		Quackable mallardThree  = duckFactory.createMallardDuck();
		Quackable mallardFour   = duckFactory.createMallardDuck();

		flockOfMallrds.add(mallardOne);
		flockOfMallrds.add(mallardTwo);
		flockOfMallrds.add(mallardThree);
		flockOfMallrds.add(mallardFour);

		flockDucks.add(flockOfMallrds);

		System.out.println("��������� ���� � ����������� �����������");

		Quackologist quackologist = new Quackologist();
		flockDucks.registerObserver(quackologist);

		simulate(flockDucks);

//		System.out.println("���������� ��� ����:");
//		simulate(flockDucks);
//
//		System.out.println("���������� ������ ���� �����:");
//		simulate(flockOfMallrds);


//		simulate(mallardDuck);
//		simulate(redheadDuck);
//		simulate(duckCall);
//		simulate(rubberDuck);
//		gooseDuck.quack();

		System.out.println("���� ��������: " + QuackCounter.getQuacks() + " ���");
	}


	public void simulate(Quackable duck) {
		duck.quack();
	}
}
