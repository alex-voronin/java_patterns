package Pattern14_01_PatternsHell_MVC_start;


/**
 * ����������� ��� ����
 * Created by a.voronin on 10.11.2015.
 */
public interface QuackObservable {

	public void registerObserver(Observer observer);

	public void notifyObservers();  // ���������� ������������
}
