package Pattern14_01_PatternsHell_MVC_start;

/**
 * ������ �����, ���� �������
 * Created by a.voronin on 09.11.2015.
 */
public class DuckCall implements Quackable {
	Observable observable;

	public DuckCall() {
		observable = new Observable(this);
	}

	@Override
	public void quack() {
		System.out.println("CallQuack!");   // ���� ������� �����
		notifyObservers();
	}


	@Override
	public void registerObserver(Observer observer) {
		observable.registerObserver(observer);
	}

	@Override
	public void notifyObservers() {
		observable.notifyObservers();
	}

	@Override
	public String toString() {
		return "DuckCall";
	}
}
