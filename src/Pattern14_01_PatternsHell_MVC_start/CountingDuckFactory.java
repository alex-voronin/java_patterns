package Pattern14_01_PatternsHell_MVC_start;

/**
 * ������� � ������������
 * Created by a.voronin on 09.11.2015.
 */
public class CountingDuckFactory extends AbstractDuckFactory {
	@Override
	public Quackable createMallardDuck() {
		return new QuackCounter(new MallardDuck());
	}

	@Override
	public Quackable createRedheadDuck() {
		return new QuackCounter(new RedheadDuck());
	}

	@Override
	public Quackable createDuckCall() {
		return new QuackCounter(new DuckCall());
	}

	@Override
	public Quackable createRubberDuck() {
		return new QuackCounter(new RubberDuck());
	}
}
