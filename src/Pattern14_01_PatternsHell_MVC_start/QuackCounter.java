package Pattern14_01_PatternsHell_MVC_start;

/**
 * ����� ��� �������� ���������� ������, ���������� ������� ���������
 * Created by a.voronin on 09.11.2015.
 */
public class QuackCounter implements Quackable {
	static int numberOfQuacks;
	Quackable duck;

	public QuackCounter(Quackable duck) {
		this.duck = duck;   // �������� ������ �� ������������ ����������
	}


	/**
	 * ����� ���������� ���������� ������ �� ���� ����������� Quackable
	 *
	 * @return
	 */
	public static int getQuacks() {
		return numberOfQuacks;
	}

	@Override
	public void quack() {
		duck.quack();
		numberOfQuacks++;
	}

	@Override
	public void registerObserver(Observer observer) {
		duck.registerObserver(observer);
	}

	@Override
	public void notifyObservers() {
		duck.notifyObservers();
	}

}
