package Pattern14_01_PatternsHell_MVC_start;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * вспомогательный класс для наблюдателя
 * Created by a.voronin on 10.11.2015.
 */
public class Observable implements QuackObservable {
	ArrayList observers = new ArrayList();
	QuackObservable duck;

	public Observable(QuackObservable duck) {
		this.duck = duck;
	}

	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void notifyObservers() {
		Iterator iterator = observers.iterator();
		while (iterator.hasNext()) {
			Observer observer = (Observer) iterator.next();
			observer.update(duck);
		}

	}

	public Iterator getObservers() {
		return observers.iterator();
	}
}
