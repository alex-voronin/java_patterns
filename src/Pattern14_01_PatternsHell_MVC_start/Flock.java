package Pattern14_01_PatternsHell_MVC_start;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ������ ����
 * Created by a.voronin on 10.11.2015.
 */
public class Flock implements Quackable {
	ArrayList ducks = new ArrayList();

	public void add(Quackable duck) {
		ducks.add(duck);
	}

	public void quack() {
		Iterator iterator = ducks.iterator();
		while (iterator.hasNext()) {
			Quackable duck = (Quackable)iterator.next();
			duck.quack();
		}
	}

	public void registerObserver(Observer observer) {
		Iterator iterator = ducks.iterator();
		while (iterator.hasNext()) {
			Quackable duck = (Quackable)iterator.next();
			duck.registerObserver(observer);
		}
	}

	public void notifyObservers() { }

	public String toString() {
		return "Flock of Ducks";
	}
}
