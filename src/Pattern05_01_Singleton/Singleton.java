package Pattern05_01_Singleton;

import java.security.PrivateKey;

/**
 * ���������
 * Created by a.voronin on 21.10.2015.
 */
public class Singleton {

	private static Singleton iniqueInstance = null; // ���������� ��� �������� ����������

	// ����������� ������
	private Singleton(){}

	public static Singleton getInstance(){
		if (iniqueInstance == null){
			iniqueInstance = new Singleton();
		}
		return  iniqueInstance;
	}


}
