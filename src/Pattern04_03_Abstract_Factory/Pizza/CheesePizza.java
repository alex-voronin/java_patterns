package Pattern04_03_Abstract_Factory.Pizza;

import Pattern04_03_Abstract_Factory.Inredients.PizzaIngredientFactory;

public class CheesePizza extends Pizza {
	PizzaIngredientFactory ingredientFactory;
 
	public CheesePizza(PizzaIngredientFactory ingredientFactory) {
		this.ingredientFactory = ingredientFactory; // �������� ������� ������������
	}

	public void prepare() {
		System.out.println("Preparing " + name);

		// � ������ ����� ����������� ���������� ������������ �� �������
		dough = ingredientFactory.createDough();
		sauce = ingredientFactory.createSauce();
		cheese = ingredientFactory.createCheese();
	}
}
