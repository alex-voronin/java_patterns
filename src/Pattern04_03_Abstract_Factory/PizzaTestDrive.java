package Pattern04_03_Abstract_Factory;

import Pattern04_03_Abstract_Factory.Pizza.Pizza;
import Pattern04_03_Abstract_Factory.Store.ChicagoPizzaStore;
import Pattern04_03_Abstract_Factory.Store.NYPizzaStore;
import Pattern04_03_Abstract_Factory.Store.PizzaStore;

public class PizzaTestDrive {
 
	public static void main(String[] args) {
		PizzaStore nyStore = new NYPizzaStore();            // ������� �������
		PizzaStore chicagoStore = new ChicagoPizzaStore();  // ������� �������
 
		Pizza pizza = nyStore.orderPizza("cheese");         // ���������� � �������� �����
		System.out.println("Ethan ordered a " + pizza + "\n");
 
		pizza = chicagoStore.orderPizza("cheese");
		System.out.println("Joel ordered a " + pizza + "\n");

		pizza = nyStore.orderPizza("clam");
		System.out.println("Ethan ordered a " + pizza + "\n");
 
		pizza = chicagoStore.orderPizza("clam");
		System.out.println("Joel ordered a " + pizza + "\n");

		pizza = nyStore.orderPizza("pepperoni");
		System.out.println("Ethan ordered a " + pizza + "\n");
 
		pizza = chicagoStore.orderPizza("pepperoni");
		System.out.println("Joel ordered a " + pizza + "\n");

		pizza = nyStore.orderPizza("veggie");
		System.out.println("Ethan ordered a " + pizza + "\n");
 
		pizza = chicagoStore.orderPizza("veggie");
		System.out.println("Joel ordered a " + pizza + "\n");
	}
}
