package Pattern04_03_Abstract_Factory.Store;

import Pattern04_03_Abstract_Factory.Inredients.NYPizzaIngredientFactory;
import Pattern04_03_Abstract_Factory.Inredients.PizzaIngredientFactory;
import Pattern04_03_Abstract_Factory.Pizza.*;

public class NYPizzaStore extends PizzaStore {

	// ��������� ����������� ����� �������� �����
	protected Pizza createPizza(String item) {
		Pizza pizza = null;

		// �������� NY ������� ������������
		PizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

		// ���������� ����� ����� ����� ��������
		if (item.equals("cheese")) {

			// ������� ����� ����� � �������� �� ������� ������������
			pizza = new CheesePizza(ingredientFactory);
			pizza.setName("New York Style Cheese Pizza");
  
		} else if (item.equals("veggie")) {
 
			pizza = new VeggiePizza(ingredientFactory);
			pizza.setName("New York Style Veggie Pizza");
 
		} else if (item.equals("clam")) {
 
			pizza = new ClamPizza(ingredientFactory);
			pizza.setName("New York Style Clam Pizza");
 
		} else if (item.equals("pepperoni")) {

			pizza = new PepperoniPizza(ingredientFactory);
			pizza.setName("New York Style Pepperoni Pizza");
 
		} 
		return pizza;
	}
}
