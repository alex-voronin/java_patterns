package Pattern04_03_Abstract_Factory.Inredients;

import Pattern04_03_Abstract_Factory.Inredients.Veggies;

public class Mushroom implements Veggies {

	public String toString() {
		return "Mushrooms";
	}
}
