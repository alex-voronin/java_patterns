package Pattern04_03_Abstract_Factory.Inredients;

public class ParmesanCheese implements Cheese {

	public String toString() {
		return "Shredded Parmesan";
	}
}
