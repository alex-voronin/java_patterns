package Pattern04_03_Abstract_Factory.Inredients;

public interface Sauce {
	public String toString();
}
