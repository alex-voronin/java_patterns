package Pattern04_03_Abstract_Factory.Inredients;

import Pattern04_03_Abstract_Factory.Inredients.Clams;

public class FrozenClams implements Clams {

	public String toString() {
		return "Frozen Clams from Chesapeake Bay";
	}
}
