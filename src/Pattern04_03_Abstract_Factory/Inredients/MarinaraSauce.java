package Pattern04_03_Abstract_Factory.Inredients;

import Pattern04_03_Abstract_Factory.Inredients.Sauce;

public class MarinaraSauce implements Sauce {
	public String toString() {
		return "Marinara Sauce";
	}
}
