package Pattern04_03_Abstract_Factory.Inredients;

public class PlumTomatoSauce implements Sauce {
	public String toString() {
		return "Tomato sauce with plum tomatoes";
	}
}
