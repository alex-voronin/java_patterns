package Pattern04_03_Abstract_Factory.Inredients;

public class ThickCrustDough implements Dough {
	public String toString() {
		return "ThickCrust style extra thick crust dough";
	}
}
