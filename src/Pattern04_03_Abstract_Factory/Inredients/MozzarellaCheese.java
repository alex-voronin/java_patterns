package Pattern04_03_Abstract_Factory.Inredients;

import Pattern04_03_Abstract_Factory.Inredients.Cheese;

public class MozzarellaCheese implements Cheese {

	public String toString() {
		return "Shredded Mozzarella";
	}
}
