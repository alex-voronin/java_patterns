package Pattern04_03_Abstract_Factory.Inredients;

import Pattern04_03_Abstract_Factory.Inredients.Veggies;

public class BlackOlives implements Veggies {

	public String toString() {
		return "Black Olives";
	}
}
