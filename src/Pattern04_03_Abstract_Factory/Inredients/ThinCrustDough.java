package Pattern04_03_Abstract_Factory.Inredients;

public class ThinCrustDough implements Dough {
	public String toString() {
		return "Thin Crust Dough";
	}
}
