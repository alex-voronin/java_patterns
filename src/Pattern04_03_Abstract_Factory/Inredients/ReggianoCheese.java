package Pattern04_03_Abstract_Factory.Inredients;

public class ReggianoCheese implements Cheese {

	public String toString() {
		return "Reggiano Cheese";
	}
}
