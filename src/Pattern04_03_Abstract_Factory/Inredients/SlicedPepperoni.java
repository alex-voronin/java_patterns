package Pattern04_03_Abstract_Factory.Inredients;

public class SlicedPepperoni implements Pepperoni {

	public String toString() {
		return "Sliced Pepperoni";
	}
}
