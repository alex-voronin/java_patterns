package Pattern04_03_Abstract_Factory.Inredients;

public interface Cheese {
	public String toString();
}
