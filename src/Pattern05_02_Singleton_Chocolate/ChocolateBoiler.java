package Pattern05_02_Singleton_Chocolate;

public final class ChocolateBoiler {
	private boolean empty;      // �����/����
	private boolean boiled;     // ������/�� ������

	private volatile static ChocolateBoiler uniqueInstance;  // ��������� �������

	private ChocolateBoiler() {
		empty = true;       // ����������� ������
		boiled = false;     // ����������� �� ������
	}

	// ������ ��������� ������� �� �������
	public static ChocolateBoiler getInstance() {
		if (uniqueInstance == null) {

			synchronized (ChocolateBoiler.class) {
				System.out.println("������� ����� ��������� �������");
				uniqueInstance = new ChocolateBoiler();
			}

		}
		System.out.println("������� ������������ ���������");
		return uniqueInstance;
	}

	/* ���������� ����������� */
	public void fill() {
		if (isEmpty()) {
			empty = false;      // ����������� �����
			boiled = false;     // ����������� �� ������
			// ���������� ����������� �������-���������� ������
		}
	}

	/* ����� ����� ����������, �� ���������, ��� ����������� ��	����, � �����
	�������� �� �������. ����� ����� ����� empty ����� ������������� true. */
	public void drain() {
		if (!isEmpty() && isBoiled()) {
			// drain the boiled milk and chocolate
			empty = true;
		}
	}

	/* ����� ���������� �����, �� ���������, ��� ����������� �����, �� ��� �� ������.
	����� ���������� ����� boiled ������������� true. */
	public void boil() {
		if (!isEmpty() && !isBoiled()) {
			// bring the contents to a boil
			boiled = true;
		}
	}

	// �������� �� �������
	public boolean isEmpty() {
		return empty;
	}

	// �������� �� ������
	public boolean isBoiled() {
		return boiled;
	}
}
