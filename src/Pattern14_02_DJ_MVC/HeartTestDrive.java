package Pattern14_02_DJ_MVC;

import Pattern14_02_DJ_MVC.Controller.ControllerInterface;
import Pattern14_02_DJ_MVC.Controller.HeartController;
import Pattern14_02_DJ_MVC.Model.HeartModel;

public class HeartTestDrive {

    public static void main (String[] args) {
		HeartModel heartModel = new HeartModel();
        ControllerInterface model = new HeartController(heartModel);
    }
}
