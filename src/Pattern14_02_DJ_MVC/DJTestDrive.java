package Pattern14_02_DJ_MVC;

import Pattern14_02_DJ_MVC.Controller.BeatController;
import Pattern14_02_DJ_MVC.Controller.ControllerInterface;
import Pattern14_02_DJ_MVC.Model.BeatModel;
import Pattern14_02_DJ_MVC.Model.BeatModelInterface;

public class DJTestDrive {

    public static void main (String[] args) {
        BeatModelInterface model = new BeatModel();
		ControllerInterface controller = new BeatController(model);
    }
}
