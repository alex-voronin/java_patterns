package Pattern14_02_DJ_MVC.Model;
  
public interface BeatObserver {
	void updateBeat();
}
