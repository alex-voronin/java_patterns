package Pattern14_02_DJ_MVC.Controller;

import Pattern14_02_DJ_MVC.View.DJView;
import Pattern14_02_DJ_MVC.HeartAdapter;
import Pattern14_02_DJ_MVC.Model.HeartModelInterface;

public class HeartController implements ControllerInterface {
	HeartModelInterface model;
	DJView view;
  
	public HeartController(HeartModelInterface model) {
		this.model = model;
		view = new DJView(this, new HeartAdapter(model));
        view.createView();
        view.createControls();
		view.disableStopMenuItem();
		view.disableStartMenuItem();
	}
  
	public void start() {}
 
	public void stop() {}
    
	public void increaseBPM() {}
    
	public void decreaseBPM() {}
  
 	public void setBPM(int bpm) {}
}



