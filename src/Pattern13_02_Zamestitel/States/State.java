package Pattern13_02_Zamestitel.States;

import java.io.Serializable;

/**
 * ��������� ��� ��������� ��������
 * Created by a.voronin on 28.10.2015.
 */
public interface State extends Serializable {
	void insertQuarter();

	void ejectQuarter();

	void turnCrank();

	void dispense();
}
