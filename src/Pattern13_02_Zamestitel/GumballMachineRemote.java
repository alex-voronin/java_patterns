package Pattern13_02_Zamestitel;

import Pattern13_02_Zamestitel.States.State;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by a.voronin on 02.11.2015.
 */
public interface GumballMachineRemote extends Remote {
	public int getCount() throws RemoteException;

	public String getLocation() throws RemoteException;

	public State getState() throws RemoteException;
}
