package Pattern13_04_Zamestitel;

import java.lang.reflect.Proxy;

/**
 * Created by a.voronin on 09.11.2015.
 */
public class PersonBeanImpl implements PersonBean {
	String name;
	String gender;
	String interests;
	int rating;
	int ratingCount = 0;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	/**
	 * ��������� ������� ������
	 *
	 * @return
	 */
	public int getHotOrNotRating() {
		if (ratingCount == 0) return 0;
		return (rating / ratingCount);

	}

	/**
	 * ����������� �������� ��������
	 *
	 * @param ratingCount
	 */
	public void setHotOrNotRating(int rating) {
		this.rating += rating;
		ratingCount++;
	}


}
