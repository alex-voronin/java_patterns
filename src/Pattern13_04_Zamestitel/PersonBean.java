package Pattern13_04_Zamestitel;

/**
 * ��������� ��� ���������
 * Created by a.voronin on 09.11.2015.
 */
public interface PersonBean {

	String getName();

	void setName(String name);

	String getGender();

	void setGender(String gender);

	String getInterests();

	void setInterests(String interests);

	int getHotOrNotRating();

	void setHotOrNotRating(int rating);

}
