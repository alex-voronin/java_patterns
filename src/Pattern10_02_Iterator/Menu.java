package Pattern10_02_Iterator;

import java.util.Iterator;

public interface Menu {
	public Iterator createIterator();
}
