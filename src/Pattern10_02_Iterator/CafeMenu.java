package Pattern10_02_Iterator;

import java.util.*;
import java.util.Iterator;

/**
 * Created by a.voronin on 26.10.2015.
 */
public class CafeMenu implements Menu{
	Hashtable menuItems = new Hashtable();

	public CafeMenu() {
		addItem("������ ������",
				"������� ������ ������� � ��� ����� ��� ��� ���",
				false, 3.99);
		addItem("��������� ���",
				"������� ������ ���� � ��� ����� ��� ��� ���",
				false, 3.99);
		addItem("������� �������",
				"������� ������ ������� � ��� ����� ��� ��� ���",
				false, 3.99);
	}

	// ������� ����� ������� � ��������� ��� � �������
	public void addItem(String name, String description, boolean vegetarian, double price) {
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.put(menuItem.getName(), menuItem);
	}

	public Hashtable getItems() {
		return menuItems;
	}


	@Override
	public Iterator createIterator() {
		return menuItems.values().iterator(); // �������� �������� ��� ��������, � �� ��� ���� ���������
	}
}
