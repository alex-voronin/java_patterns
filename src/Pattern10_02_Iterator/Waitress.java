package Pattern10_02_Iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * ����� "����������" ���������� ��������� ��� ������ ������ ����
 */
public class Waitress {
	ArrayList menus;
	Menu pancakeHouseMenu;
	Menu dinerMenu;
	Menu cafeMenu;

	/**
	 * ���� ����������� �������� ������ ����
	 *
	 * @param menus
	 */
	public Waitress(ArrayList menus) {
		this.menus = menus;
	}

	/**
	 * ���� ��� ������� ����
	 */
	public Waitress() {
		this.pancakeHouseMenu = new PancakeHouseMenu();
		this.dinerMenu = new DinerMenu();
		this.cafeMenu = new CafeMenu();
	}

	/**
	 * ��� �������� ���������� - �������� ��� ����, � �������� ��� ��������
	 * ���� �������� ��� ���� ��� ���������
	 *
	 * @param pancakeHouseMenu
	 * @param dinerMenu
	 * @param cafeMenu
	 */
	public Waitress(Menu pancakeHouseMenu, Menu dinerMenu, Menu cafeMenu) {
		this.pancakeHouseMenu = pancakeHouseMenu;
		this.dinerMenu = dinerMenu;
		this.cafeMenu = cafeMenu;
	}

	/**
	 * ����� ����
	 * ���������� ����� ��� �������� ���� - ��� �������� �������� � �������� ��� �
	 * ����� ������ ����
	 */
	public void printMenu() {
		Iterator pancakeIterator = pancakeHouseMenu.createIterator();
		Iterator dinerIterator = dinerMenu.createIterator();
		Iterator cafeIterator = cafeMenu.createIterator();

		System.out.println("MENU\n----\nBREAKFAST");
		printMenu(pancakeIterator);

		System.out.println("\nLUNCH");
		printMenu(dinerIterator);

		System.out.println("\nDINNER");
		printMenu(cafeIterator);
	}

	public void printMenuNew() {
		Iterator menuIterator = menus.iterator();
		while (menuIterator.hasNext()) {
			Menu menu = (Menu)menuIterator.next();
			printMenu(menu.createIterator());
		}
	}

	/**
	 * ����� ������ ���� - ����� ������� ����� ����, �� �������� �� ���������� ����������
	 * ��� ��� ��� ��� ������� ����� ��������, ������� ��������
	 *
	 * @param iterator - �������� ��� ����������� ����
	 */
	private void printMenu(Iterator iterator) {
		while (iterator.hasNext()) {
			MenuItem menuItem = (MenuItem) iterator.next();
			System.out.print(menuItem.getName() + ", ");
			System.out.print(menuItem.getPrice() + " -- ");
			System.out.println(menuItem.getDescription());
		}
	}
}
