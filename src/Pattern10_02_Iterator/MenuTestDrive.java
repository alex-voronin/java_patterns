package Pattern10_02_Iterator;

import java.util.ArrayList;

/**
 * ������ ������� ��������
 */
public class MenuTestDrive {
	public static void main(String args[]) {

		// ������� ����, �� ��� ����� ��������� ������ ����������
		// ����� �� � ���������� ������ ������ �� ������� �� ����,
		// �� ���������� ����������

       /* PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
        DinerMenu dinerMenu = new DinerMenu();
        CafeMenu cafeMenu = new CafeMenu();*/

		// ������� ����������
		//Waitress waitress = new Waitress();

//      �������� ���� � ������ � �������� ������������ ����������
		ArrayList menus = new ArrayList();
		menus.add(new PancakeHouseMenu());
		menus.add(new DinerMenu());
		menus.add(new CafeMenu());

		Waitress waitress = new Waitress(menus);

		// ������ �� ������� ������ ���� �� �����
		waitress.printMenuNew();
	}
}
