package Pattern10_02_Iterator;

//import Pattern10_01_Iterator.Iterator;

//import Pattern10_02_Iterator.MenuItem;

import java.util.Iterator;

/**
 * Created by a.voronin on 23.10.2015.
 */
public class DinerMenuIterator implements Iterator {
	MenuItem[] list;
	int position = 0;


	public DinerMenuIterator(MenuItem[] list) {
		this.list = list;
	}

	/**
	 * ��������� ���� �� ��� �������� �������
	 *
	 * @return
	 */
	@Override
	public boolean hasNext() {
		if (position >= list.length || list[position] == null) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * ���������� ��������� ������� �������
	 *
	 * @return
	 */
	@Override
	public Object next() {
		MenuItem menuItem = list[position];
		position = position + 1;
		return menuItem;

	}

	public void remove() {
		if (position <= 0) {
			throw new IllegalStateException("Cant remove");
		}

		if (list[position - 1] != null) {
			for (int i = position - 1; i < (list.length - 1); i++) {
				list[i] = list[i + 1];
			}
			list[list.length - 1] = null;
		}
	}

}
