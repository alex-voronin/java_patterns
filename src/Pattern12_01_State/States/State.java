package Pattern12_01_State.States;

/**
 * ��������� ��� ��������� ��������
 * Created by a.voronin on 28.10.2015.
 */
public interface State {
	void insertQuarter();

	void ejectQuarter();

	void turnCrank();

	void dispense();
}
