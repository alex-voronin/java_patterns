package Pattern12_01_State;

public class GumballMachineTestDrive {

	public static void main(String[] args) {
		int count = 0;

		String[] tmp_args = new String[2];
		tmp_args[0] = "������ ������� ������� �� ��������";
		tmp_args[1] = "20";

//		if (args.length < 2){
//			System.out.println("GumballMachine <name> <inventory>");
//			System.exit(1);
//		}

		count = Integer.parseInt(tmp_args[1]);

		GumballMachine gumballMachine = new GumballMachine(tmp_args[0], count);

		GumballMonitor monitor = new GumballMonitor(gumballMachine);

		System.out.println(gumballMachine);

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();

		System.out.println(gumballMachine);

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();

		System.out.println(gumballMachine);

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();

		System.out.println(gumballMachine);

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();

		System.out.println(gumballMachine);

		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();
		gumballMachine.insertQuarter();
		gumballMachine.turnCrank();

		System.out.println(gumballMachine);

		monitor.report();
	}
}
