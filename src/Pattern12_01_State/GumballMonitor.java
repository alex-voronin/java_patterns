package Pattern12_01_State;

/**
 * Created by a.voronin on 02.11.2015.
 */
public class GumballMonitor {
	GumballMachine machine;

	public GumballMonitor(GumballMachine machine) {
		this.machine = machine;
	}

	public void report() {
		System.out.println("Gumball Machine:    " + machine.getLocation());
		System.out.println("Current inventory:  " + machine.getCount() + " gumballs");
		System.out.println("Current state:      " + machine.getState());

	}
}
