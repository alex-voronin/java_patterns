package Pattern04_02_Factory_Method;

import Pattern04_02_Factory_Method.Pizza.*;

/**
 * ������� ������� ��� �����, ����� �������� � ����������� �����
 * Created by a.voronin on 19.10.2015.
 */
public class SimplePizzaFactory {

	public Pizza createPizza(String type){
		Pizza pizza = null;

		if(type.equals("cheese")) {
			pizza = new CheesePizza();
		} else if (type.equals("pepperoni")) {
			pizza = new PepperoniPizza();
		} else if (type.equals("clam")) {
			pizza = new ClamPizza();
		} else if (type.equals("veggie")) {
			pizza = new VeggiePizza();
		}
		return pizza;
	}
}
