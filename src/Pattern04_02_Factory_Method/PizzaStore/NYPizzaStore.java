package Pattern04_02_Factory_Method.PizzaStore;

import Pattern04_02_Factory_Method.Pizza.Pizza;

/**
 * Created by a.voronin on 20.10.2015.
 */
public class NYPizzaStore extends PizzaStore {

	public Pizza createPizza(String type) {
		Pizza pizza = null;
		if (type.equals("cheese")) {
			pizza = new NYCheesePizza();
		} else if (type.equals("peperoni")) {
			//pizza = new NYPeperoniPizza();
		} else if (type.equals("clam")) {
			//pizza = new NYClamPizza();
		}
		return pizza;
	}
}
