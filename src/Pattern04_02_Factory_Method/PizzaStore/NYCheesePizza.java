package Pattern04_02_Factory_Method.PizzaStore;

import Pattern04_02_Factory_Method.Pizza.Pizza;

/**
 * Created by a.voronin on 20.10.2015.
 */
public class NYCheesePizza extends Pizza {

	public NYCheesePizza() {
		name = "NY Style Sauce and Cheese Pizza";
		dough = "Thin Crust Dough";
		sauce = "Marinara Sauce";

		toppings.add("Grated Reggiano Cheese");
		toppings.add("Nuts");
	}

	public void cut() {
		System.out.println("Cutting the pizza into square slices");
	}

}
