package Pattern04_02_Factory_Method.PizzaStore;

import Pattern04_02_Factory_Method.Pizza.Pizza;

/**
 * ������������ ������ ���������� ����� ������ �����, ��� �������� ���������������
 * � ����� createPizza(), �� ����� �������� �� ���������� �������� �����
 * ��� ����� ������ �������� ������ �������������� ���������� ���� ����� createPizza()
 */
public abstract class PizzaStore {
	//SimplePizzaFactory factory;

	// ����������� �������� �������, ��� ��������
	//public PizzaStore(SimplePizzaFactory factory) {
		//this.factory = factory;
	//}


	// ������� ����� ��� ���� ������� � ������, ��� ��������� ��� ���� ���������
	// �� ��, � ����� ����� ������ �����, ������ ������ ���������� �������
	public Pizza orderPizza(String type) {
		Pizza pizza = createPizza(type);

		//pizza = createPizza(type);      //factory.createPizza(type);
		pizza.prepare();
		pizza.bake();
		pizza.cut();
		pizza.box();

		return pizza;
	}

	// ��������� ����� ���� �����������
	// �� ����������� ��������� � ������ ������ ���� ����� ����� ��������
	protected abstract Pizza createPizza(String item);

}
