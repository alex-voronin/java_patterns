package Pattern04_02_Factory_Method;

import Pattern04_02_Factory_Method.Pizza.Pizza;
import Pattern04_02_Factory_Method.PizzaStore.NYPizzaStore;
import Pattern04_02_Factory_Method.PizzaStore.PizzaStore;

public class PizzaTestDrive {
 
	public static void main(String[] args) {
		/*
		SimplePizzaFactory factory = new SimplePizzaFactory();
		PizzaStore store = new PizzaStore(factory);

		Pizza pizza = store.orderPizza("cheese");
		System.out.println("We ordered a " + pizza.getName() + "\n");
 
		pizza = store.orderPizza("veggie");
		System.out.println("We ordered a " + pizza.getName() + "\n");
		*/

		PizzaStore nyStore = new NYPizzaStore();        // �������
		Pizza pizza = nyStore.orderPizza("cheese");     // �����
		System.out.println("Ethan ordered a " + pizza.getName() + "\n");
	}
}
