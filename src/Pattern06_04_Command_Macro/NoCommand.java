package Pattern06_04_Command_Macro;


/**
 * Created by a.voronin on 21.10.2015.
 */
public class NoCommand implements Command {
	public void execute() { }

	public void undo() { }
}
