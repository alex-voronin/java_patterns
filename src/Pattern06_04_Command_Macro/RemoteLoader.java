package Pattern06_04_Command_Macro;

/**
 * Created by a.voronin on 22.10.2015.
 */
public class RemoteLoader {
	public static void main(String[] args) {
		RemoteControl remoteControl = new RemoteControl();
		CeilingFan ceilingFan = new CeilingFan("Living Room");

		// ������� �������
		CeilingFanMediumCommand ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
		CeilingFanHighCommand ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
		CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);

		// ������ �������
		remoteControl.setCommand(0, ceilingFanMedium, ceilingFanOff);
		remoteControl.setCommand(1, ceilingFanHigh, ceilingFanOff);


		remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		//System.out.println(remoteControl);
		System.out.println("������");
//		remoteControl.undoButtonWasPushed();

		remoteControl.onButtonWasPushed(1);
		//System.out.println(remoteControl);
		System.out.println("������");
//		remoteControl.undoButtonWasPushed();


		System.out.println(remoteControl);
		System.out.println(" --Pushing Macro On--");

//		������ ���� ��� ������������� �����������, ����������� ����� � �������
//		��.����� MacroCommand
		/*
		Command [] partyOn = { lightOn, stereoOn, tvOn, hottubOn};
		Command [] partyOff = { lightOff, stereoOff, tvOff, hottubOff};
		MacroCommand partyOnMacro = new MacroCommand (partyOn) ;
		MacroCommand partyOffMacro = new MacroCommand(partyOff );

		remoteControl .setCommand (0, partyOnMacro, partyOffMacro)

		remoteControl.onButtonWasPushed(0);
		System.out.println("--Pushing Macro Off--");
		remoteControl.offButtonWasPushed(0);
		*/


	}

}
