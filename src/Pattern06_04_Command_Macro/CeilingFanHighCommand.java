package Pattern06_04_Command_Macro;

/**
 * Created by a.voronin on 22.10.2015.
 */
public class CeilingFanHighCommand implements Command {
	CeilingFan ceilingFan;
	int prevSpeed;              // ���������� ��������

	public CeilingFanHighCommand(CeilingFan ceilingFan) {
		this.ceilingFan = ceilingFan;
	}

	public void execute() {
		prevSpeed = ceilingFan.getSpeed();      // �������� ����.��������
		ceilingFan.high();                      // ������������� ����� ��������
	}

	/**
	 * ���������� ���������� � ���������� ��������
	 */
	public void undo() {
		if (prevSpeed == CeilingFan.HIGH) {
			ceilingFan.high();

		} else if (prevSpeed == CeilingFan.MEDIUM) {
			ceilingFan.medium();

		} else if (prevSpeed == CeilingFan.LOW) {
			ceilingFan.low();

		} else if (prevSpeed == CeilingFan.OFF) {
			ceilingFan.off();
		}
	}

}
