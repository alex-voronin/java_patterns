package Pattern06_04_Command_Macro;

/**
 * �����-�������, ������� ��������� ������ �������
 * ��� ����������� �� ������
 * Created by a.voronin on 22.10.2015.
 */
public class MacroCommand implements Command {
	Command[] commands;

	/**
	 * ��������� ������ ������
	 *
	 * @param commands - ������ ������
	 */
	public MacroCommand(Command[] commands) {
		this.commands = commands;
	}

	/**
	 * ���������� ���� ������ �� �������
	 */
	public void execute() {
		for (int i = 0; i < commands.length; i++) {
			commands[i].execute();
		}
	}


}
