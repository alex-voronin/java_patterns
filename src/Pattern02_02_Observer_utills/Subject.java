package Pattern02_02_Observer_utills;

/**
 * ��������� ��������
 * Created by a.voronin on 15.10.2015.
 */
public interface Subject {
	public void registerObserver(Observer o);   // �������������� �����������
	public void removeObserver(Observer o);     // ������� �����������
	public void notifyObservers();              // ��������� ������������ �����������
}
