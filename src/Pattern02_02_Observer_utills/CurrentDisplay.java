package Pattern02_02_Observer_utills;

import java.util.*;

/**
 * ����� �������� ������ �� WeatherData
 * Created by a.voronin on 15.10.2015.
 */
public class CurrentDisplay implements java.util.Observer, DisplayElement {
	Observable observable;
	private float temperature = 0;
	private float humidity = 0;
	//private Subject weatherData;

	public CurrentDisplay(Observable observable) {
		this.observable = observable;
		observable.addObserver(this);
	}

	/*
	public void update(float temperature, float humidity, float pressure) {
		this.temperature = temperature;     // ��������� ��������, �����
		this.humidity = humidity;
		display();                          // ��������
	}
	*/

	public void display() {
		System.out.println("Current conditions: " + temperature
				+ "F degrees and " + humidity + "% humidity");
	}


	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof WeatherData) {
			WeatherData weatherData = (WeatherData) o;
			this.temperature = weatherData.getTemperature();
			this.humidity = weatherData.getHumidity();

			display();
		}
	}

}
