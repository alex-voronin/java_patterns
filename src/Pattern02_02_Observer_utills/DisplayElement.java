package Pattern02_02_Observer_utills;

/**
 * Ёлемент, отображающий данные
 * Created by a.voronin on 15.10.2015.
 */
public interface DisplayElement {
	public void display();    // отображение

}
