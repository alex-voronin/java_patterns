package Pattern06_03_Command_Fan;


/**
 * Created by a.voronin on 21.10.2015.
 */
public class NoCommand implements Command {
	public void execute() { }

	public void undo() { }
}
