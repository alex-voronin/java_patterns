package Pattern06_03_Command_Fan;

/**
 * Created by a.voronin on 22.10.2015.
 */
public class RemoteLoader {
	public static void main(String[] args) {
		RemoteControl remoteControl = new RemoteControl();
		CeilingFan ceilingFan = new CeilingFan("Living Room");

		// ������� �������
		CeilingFanMediumCommand ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
		CeilingFanHighCommand ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
		CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan);

		// ������ �������
		remoteControl.setCommand(0, ceilingFanMedium, ceilingFanOff);
		remoteControl.setCommand(1, ceilingFanHigh, ceilingFanOff);


		remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		//System.out.println(remoteControl);
		System.out.println("������");
		remoteControl.undoButtonWasPushed();

		remoteControl.onButtonWasPushed(1);
		//System.out.println(remoteControl);
		System.out.println("������");
		remoteControl.undoButtonWasPushed();

	}

}
